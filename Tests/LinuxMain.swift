import XCTest

import RGF_MonitorTests

var tests = [XCTestCaseEntry]()
tests += RGF_MonitorTests.allTests()
XCTMain(tests)