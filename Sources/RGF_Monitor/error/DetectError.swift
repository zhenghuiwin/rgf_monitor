//
//  DetectError.swift
//  RGF_Monitor
//
//  Created by zhenghuiwin on 2018/10/15.
//

import Foundation

enum DetectError: Error {
    case NoFileNumInfo
    case NoLogFile
    case FailedToConvertToData
    
    var localizedDescription: String {
        switch self {
        case .NoFileNumInfo:
            return "Failed to get the number infomation of specific file"
        case .NoLogFile:
            return "Can not open the log file"
        case .FailedToConvertToData:
            return "FailedToConvertToData"
        }
    }
}
