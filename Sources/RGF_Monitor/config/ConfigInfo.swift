//
//  Config.swift
//  RGF_Monitor
//
//  Created by zhenghuiwin on 2018/10/14.
//

import Foundation
import PerfectLib

struct ConfigInfo: Codable {
    
    struct SSHInfo: Codable {
        let host: String
        let port: Int
        let user: String
        let password: String
    }
    
    struct TargetFile: Codable {
        let directory: String
        let fileNameTemplate: String
    }
    
    struct RGF: Codable {
        let scriptPath: String
    }
    
    struct CheckPoint: Codable {
        // Maximun time the expect file dose not exist. Unit: Minutes
        let validTimeRange: Int
    }
    
    let ssh: SSHInfo
    let targetFile: TargetFile
    let rgf: RGF
    let checkPoint: CheckPoint
    
   
}
