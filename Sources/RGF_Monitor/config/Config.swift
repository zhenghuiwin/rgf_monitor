//
//  Config.swift
//  RGF_Monitor
//
//  Created by zhenghuiwin on 2018/10/14.
//

import Foundation

class Config {
    
    public class func load() throws -> ConfigInfo {
        
        let fileMgr = FileManager.default
        let configPath = "\(fileMgr.currentDirectoryPath)/conf/config.json"
        print("Config#load:\(configPath)")
        
        let configData = try Data(contentsOf: URL(fileURLWithPath: configPath))
    
        
        let jsonDecoder = JSONDecoder()
        let configInfo = try jsonDecoder.decode(ConfigInfo.self, from: configData)
        
        return configInfo
    }
}
