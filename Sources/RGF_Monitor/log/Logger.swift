//
//  Logger.swift
//  RGF_Monitor
//
//  Created by zhenghuiwin on 2018/10/15.
//

import Foundation


class Logger {
    
    static let shared = Logger()
    
    var logPath: String = ""
    
    var timeFormat: String = "yyyy-MM-dd HH:mm:ss zzz" {
        didSet(newValue){
            dateFormatter.dateFormat = newValue
        }
    }
    
    private var fileHandle: FileHandle?
    
    private var dateFormatter = DateFormatter()
    
    private init() {
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
        dateFormatter.timeZone = TimeZone.current
        logPath = FileManager.default.currentDirectoryPath + "/log/monitor.log"
    }
    

    public func info(msg: String) {
        let strDate = dateFormatter.string(from: Date())
        let logMsg = "\(strDate) INFO: \(msg)"
        
        print(logMsg)
        
        do {
            try saveToFile(msg: logMsg)
        } catch let e as DetectError {
            print("Logger#info:DetctError\(e.localizedDescription)")
        }catch let e {
            print("Logger#info: \(e.localizedDescription)")
        }
    }
    
    private func saveToFile(msg: String) throws {
        
        let logLine = msg + "\r\n"
        
        let fileMgr = FileManager.default
        if !fileMgr.fileExists(atPath: logPath) {
            try logLine.write(to: URL(fileURLWithPath: logPath), atomically: true, encoding: String.Encoding.utf8)
        } else {
            if fileHandle == nil  {
                 fileHandle = FileHandle(forWritingAtPath: logPath)
            }
            
            guard let fileHandle = fileHandle else {
                throw DetectError.NoLogFile
            }
            
            fileHandle.seekToEndOfFile()
            
            guard let data = logLine.data(using: String.Encoding.utf8) else {
                throw DetectError.FailedToConvertToData
            }
        
            fileHandle.write(data)
        }
    }
    
    deinit {
        // close file handle if set
        if let fileHandle = fileHandle {
            fileHandle.closeFile()
        }
    }
    
}
