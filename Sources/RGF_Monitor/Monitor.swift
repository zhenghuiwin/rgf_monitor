//
//  Monitor.swift
//  RGF_Monitor
//
//  Created by zhenghuiwin on 2018/10/15.
//

import Foundation
import Shout

class Monitor {
    
    private static let FILE_TIME_EXP = "{time}"
    
    // 45 minutes
//    private static let EXPIRE_INTERVAL: Double = 45 * 60
    
    private let log = Logger.shared
    
    public func inspect(config: ConfigInfo) throws {
        
        let ssh = try SSH(host: config.ssh.host, port: Int32(config.ssh.port))
        
        defer {
            exitSSHConnection(ssh: ssh)
            log.info(msg: "Closed the SSH connection.")
        }
        
        try ssh.authenticate(username: config.ssh.user, password: config.ssh.password)
        
        
        log.info(msg: "Connected to the host: [\(config.ssh.host)].")
        
        
        var rgfid: String? = try findRGFProcess(ssh: ssh)
        guard let rgfPid = rgfid else {
            log.info(msg: "No RGF process is running.")
            try runRGF(atPath: config.rgf.scriptPath, ssh: ssh)
            
            rgfid = try findRGFProcess(ssh: ssh)
            if let rgfPid = rgfid {
                log.info(msg: "RGF [\(rgfPid)] is running.")
            }
            
            return
        }
        
        log.info(msg: "The RGF [\(rgfPid)] is running.")
        
        let (fileExist, expectedFileTime) = try checkExpectedFileExisting(config: config, ssh: ssh)
        
        if fileExist {
            log.info(msg: "The expected file has already existed.")
            return
        }
        
        log.info(msg: "The expected file which time is \(expectedFileTime) is not exist.")
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmm0000"
        guard let expectedDate = formatter.date(from: expectedFileTime) else {
            log.info(msg: "Failed to convert \(expectedFileTime) to Date!")
            return
        }
        
        let current = Date()
        let intervalSeconds = current.timeIntervalSince(expectedDate)
        
        if intervalSeconds.isLess(than: Double(config.checkPoint.validTimeRange)) {
            log.info(msg: "Not exceeding the valid time range.")
            return
        }
        

        log.info(msg: "The interval to the time of expected file is out of the range.")
        
        log.info(msg: "Will terminate the RGF process if it is running.")
        try terminateRGFProcess(rgfPid: rgfPid, ssh: ssh)
        
        log.info(msg: "Start to run RGF.")
        try runRGF(atPath: config.rgf.scriptPath, ssh: ssh)
        
    }
    
    private func runRGF(atPath path: String, ssh: SSH) throws {

        let cmdPwd = "cd \(path) && nohup ./run.sh &"
        do {
            let statPwd = try ssh.execute(cmdPwd, output: { (out) in
                log.info(msg: "output of '\(cmdPwd)' is \(out).")
                do {
                    if let rgfPid = try findRGFProcess(ssh: ssh) {
                        log.info(msg: "[\(rgfPid)] is running.")
                    }
                    
                    log.info(msg: "Program will exit.")
                    exit(0)
                } catch let e {
                     log.info(msg: "Error: \(e)")
                }
                
            })
            log.info(msg: "stat of '\(cmdPwd)' is \(statPwd).")
        } catch let e {
            log.info(msg: "Error: \(e)")
        }
    }
    
    private func terminateRGFProcess(rgfPid: String, ssh: SSH) throws {
    
        try terminateProcess(id: rgfPid, ssh: ssh)
        
        log.info(msg: "kill command has been executed.")

    }
    
    private func terminateProcess(id: String, ssh: SSH) throws {
        let cmd = "kill -9 \(id)"
        try ssh.execute(cmd)
    }
    
    private func findRGFProcess(ssh: SSH) throws -> String? {
        let keyCmd = "java -server -Xmx64g -classpath /home/hxf/calf241/"
        let processIDCmd  = "ps -ef | grep '\(keyCmd)'"
        
        let (_, output) = try ssh.capture(processIDCmd)
        
        var targetOut: Substring? = nil
        output.split(separator: "\n").forEach { out in
            let replacedOut = out.replacingOccurrences(of: "'", with: "")
            if !replacedOut.hasSuffix("\(keyCmd)") {
                targetOut = out
                return
            }
        }
        
        guard let target = targetOut,
            target.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "" else {
                return nil
        }
        
        print("targt: \(target)")
        
        let outParts = target.split(separator: " ")
        guard outParts.count >= 2 else {
            log.info(msg: "Not enough infomation to get the process ID of RGF.")
            return nil
        }
        
        let proccessID = outParts[1]
        
        return String(proccessID)
    }
    
    func exitSSHConnection(ssh: SSH)  {
        do {
            try ssh.execute("exit")
        } catch let e {
            log.info(msg: e.localizedDescription)
        }
    }
    
    
    /// Check wether the expected file is exist
    ///
    /// - Parameter config: configuraton infomation
    /// - Returns: true: the file is exist,and the string is the epected time(yyyyMMddHH0000)
    ;
    ///            false: no file is exist;
    /// - Throws: DetectError.NoFileNumInfo
    private func checkExpectedFileExisting(config: ConfigInfo, ssh: SSH) throws -> (Bool, String) {
        let (year, monthAndDay, hour) = expectedTime()
        let filePath = targetFile(year: year, monthAndDay: monthAndDay, hour: hour, config: config)
        
        let cmd = "ls \(filePath) | wc -l"
        
        print("Checking file cmd: \(cmd)")
        
        let (_, output) = try ssh.capture(cmd)
        guard let fileNum = Int(output.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)) else {
            throw DetectError.NoFileNumInfo
        }
        
        print("Number of expected file: \(fileNum)")
        
        
        return (fileNum > 0, year + monthAndDay + hour + "0000")
    }
    
    /// Generate the expected time of the NCFile product date
    ///
    /// - Returns: (yyyy, MMdd, HH)
    private func expectedTime() -> (String, String, String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MMdd HH:mm:ss"
        
        let currentDate: String = dateFormatter.string(from: Date())
        
        // Be splited in "yyyy-MMdd" and  "HH:mm:ss"
        let dateParts: [Substring] = currentDate.split(separator: " ")
        
        // "yyyy" and "MMdd"
        let yearAndMonthDay: [Substring]  = dateParts[0].split(separator: "-")
        // "HH" and "mm" and "ss"
        let hourAndMinute:   [Substring]  = dateParts[1].split(separator: ":")
        
        let year = yearAndMonthDay[0]
        let monthAndDay = yearAndMonthDay[1]
        
        let hour = hourAndMinute[0]

        
        return (String(year), String(monthAndDay), String(hour))
    }
    
    
    /// The full path of target file which will be checked
    ///
    /// - Parameters:
    ///   - year: year,like "yyyy"
    ///   - monthAndDay: "MMdd"
    ///   - hour: "HH"
    /// - Returns: The full path of target file 
    private func targetFile(year: String, monthAndDay: String, hour: String, config: ConfigInfo) -> String {
        let template = config.targetFile.fileNameTemplate
        let fileName = template.replacingOccurrences(of: Monitor.FILE_TIME_EXP, with: "\(year)\(monthAndDay)\(hour)00")
        let filePath = config.targetFile.directory + year + "/" + "\(year)\(monthAndDay)/" + fileName
        
        return filePath
    }
}
